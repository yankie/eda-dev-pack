<?xml version="1.0" encoding="UTF-8"?>
<!--

 Author: Pavel V. Yefimov <p.v.yefimov@gmail.com>
 Copyright (C) 2023 Pavel V. Yefimov

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.

 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

-->
<language id="skill" _name="SKILL (Cadence)" version="2.0" _section="Scientific">
  <metadata>
    <property name="mimetypes">text/x-skill;text/x-lisp;text/x-scheme;</property>
    <property name="globs">*.il;*.il;*.tf;</property>
    <property name="line-comment-start">;</property>
  </metadata>

  <styles>
    <style id="comment" _name="Comment" map-to="def:comment"/>
    <style id="boolean" _name="Boolean" map-to="def:boolean"/>
    <style id="symbol" _name="String" map-to="def:string"/>
    <style id="string" _name="String" map-to="def:string"/>
    <style id="keyword" _name="Keyword" map-to="def:keyword"/>
    <style id="function" _name="Function" map-to="def:function"/>
    <style id="escaped-character" _name="Escaped Character" map-to="def:special-constant"/>
    <style id="printf" _name="printf Conversion" map-to="def:special-char"/>
    <style id="error" _name="Error" map-to="def:error"/>
    <style id="type" _name="Data Type" map-to="def:type"/>
  </styles>

  <definitions>

    <context id="skill">
      <include>
        <context ref="line-comment"/>
        <context ref="multiline-comment"/>
        <context ref="string"/>
        <context ref="boolean-value"/>
        <context ref="symbol"/>
        <context ref="keyword"/>
        <context ref="function"/>
        <context ref="cadence-function"/>
        <context ref="user-function"/>
        <context ref="operator"/>
        <context ref="type"/>
        <context ref="binary"/>
        <context ref="float"/>
        <context ref="def:octal"/>
        <context ref="def:hexadecimal"/>
        <context ref="decimal"/>
      </include>
    </context>


    <context id="cadence-function" style-ref="function">
      <prefix>(?&lt;![\w\d_])</prefix>
      <suffix>(?![\w\d_])(?=\()</suffix>
      <keyword>ae\w+</keyword>
      <keyword>dd\w+</keyword>
      <keyword>db\w+</keyword>
      <keyword>hi\w+</keyword>
      <keyword>pc\w+</keyword>
      <keyword>si\w+</keyword>
      <keyword>tech\w+</keyword>
      <keyword>lay\w+</keyword>
      <keyword>sch\w+</keyword>
    </context>

    <context id="user-function" style-ref="function">
      <prefix>(?&lt;![\w\d_])</prefix>
      <suffix>(?![\w\d_])(?=\()</suffix>
      <keyword>\w+</keyword>
    </context>

    <context id="type" style-ref="type">
      <keyword>boolean</keyword>
      <keyword>flonum</keyword>
      <keyword>fixnum</keyword>
      <keyword>string</keyword>
      <keyword>symbol</keyword>
      <keyword>list</keyword>
    </context>

    <context id="keyword" style-ref="keyword">
      <prefix>(?&lt;![\w\d_])</prefix>
      <suffix>(?![\w\d_])</suffix>
<!-- 10 Flow Control Functions -->
        <keyword>case</keyword>
        <keyword>caseq</keyword>
        <keyword>catch</keyword>
        <keyword>cond</keyword>
        <keyword>decode</keyword>
        <keyword>do</keyword>
        <keyword>exists</keyword>
        <keyword>existss</keyword>
        <keyword>for</keyword>
        <keyword>fors</keyword>
        <keyword>forall</keyword>
        <keyword>foralls</keyword>
        <keyword>foreach</keyword>
        <keyword>foreachs</keyword>
        <keyword>if|then|else</keyword>
        <keyword>go</keyword>
        <keyword>map</keyword>
        <keyword>mapc</keyword>
        <keyword>mapcan</keyword>
        <keyword>mapcar</keyword>
        <keyword>mapcon</keyword>
        <keyword>mapinto</keyword>
        <keyword>maplist</keyword>
        <keyword>not</keyword>
        <keyword>regExitAfter</keyword>
        <keyword>regExitBefore</keyword>
        <keyword>remExitProc</keyword>
        <keyword>return</keyword>
        <keyword>setof</keyword>
        <keyword>setofs</keyword>
        <keyword>throw</keyword>
        <keyword>unless</keyword>
        <keyword>when</keyword>
        <keyword>while</keyword>

        <!-- 11 Input Output Functions -->
        <keyword>close</keyword>
        <keyword>compress</keyword>
        <keyword>display</keyword>
        <keyword>drain</keyword>
        <keyword>ed</keyword>
        <keyword>edi</keyword>
        <keyword>edit</keyword>
        <keyword>edl</keyword>
        <keyword>encrypt</keyword>
        <keyword>expandMacroDeep</keyword>
        <keyword>fileLength</keyword>
        <keyword>fileSeek</keyword>
        <keyword>fileTell</keyword>
        <keyword>fileTimeModified</keyword>
        <keyword>fprintf</keyword>
        <keyword>fscanf|scanf|sscanf</keyword>
        <keyword>get_filename</keyword>
        <keyword>getc</keyword>
        <keyword>getDirFiles</keyword>
        <keyword>getOutstring</keyword>
        <keyword>gets</keyword>
        <keyword>include</keyword>
        <keyword>infile</keyword>
        <keyword>info</keyword>
        <keyword>inportp</keyword>
        <keyword>instring</keyword>
        <keyword>isExecutable</keyword>
        <keyword>isFile</keyword>
        <keyword>isFileEncrypted</keyword>
        <keyword>isFileName</keyword>
        <keyword>isLargeFile</keyword>
        <keyword>isLink</keyword>
        <keyword>isPortAtEOF</keyword>
        <keyword>isReadable</keyword>
        <keyword>isWritable</keyword>
        <keyword>lineread</keyword>
        <keyword>linereadstring</keyword>
        <keyword>load</keyword>
        <keyword>loadi</keyword>
        <keyword>loadPort</keyword>
        <keyword>loadstring</keyword>
        <keyword>outstring</keyword>
        <keyword>makeTempFileName</keyword>
        <keyword>newline</keyword>
        <keyword>numOpenFiles</keyword>
        <keyword>openportp</keyword>
        <keyword>outfile</keyword>
        <keyword>outportp</keyword>
        <keyword>portp</keyword>
        <keyword>pprint</keyword>
        <keyword>print</keyword>
        <keyword>printf</keyword>
        <keyword>printlev</keyword>
        <keyword>println</keyword>
        <keyword>putc</keyword>
        <keyword>read</keyword>
        <keyword>readTable</keyword>
        <keyword>renameFile</keyword>
        <keyword>simplifyFilename</keyword>
        <keyword>simplifyFilenameUnique</keyword>
        <keyword>truename</keyword>
        <keyword>which</keyword>
        <keyword>write</keyword>
        <keyword>writeTable</keyword>

        <!-- 12 Core Functions -->
        <keyword>arglist</keyword>
        <keyword>assert</keyword>
        <keyword>atom</keyword>
        <keyword>bcdp</keyword>
        <keyword>booleanp</keyword>
        <keyword>boundp</keyword>
        <keyword>describe</keyword>
        <keyword>fdoc</keyword>
        <keyword>gc</keyword>
        <keyword>gensym</keyword>
        <keyword>getMuffleWarnings</keyword>
        <keyword>getSkillVersion</keyword>
        <keyword>get_pname</keyword>
        <keyword>get_string</keyword>
        <keyword>getVersion</keyword>
        <keyword>getWarn</keyword>
        <keyword>help</keyword>
        <keyword>inScheme</keyword>
        <keyword>inSkill</keyword>
        <keyword>isVarImported</keyword>
        <keyword>makeSymbol</keyword>
        <keyword>measureTime</keyword>
        <keyword>muffleWarnings</keyword>
        <keyword>needNCells</keyword>
        <keyword>restoreFloat</keyword>
        <keyword>saveFloat</keyword>
        <keyword>schemeTopLevelEnv</keyword>
        <keyword>setPrompts</keyword>
        <keyword>sstatus</keyword>
        <keyword>status</keyword>
        <keyword>theEnvironment</keyword>
        <keyword>unbindVar</keyword>

        <!-- 13 Function and Program Structure -->
        <keyword>addDefstructClass</keyword>
        <keyword>alias</keyword>
        <keyword>apply</keyword>
        <keyword>argc</keyword>
        <keyword>argv</keyword>
        <keyword>begin</keyword>
        <keyword>clearExitProcs</keyword>
        <keyword>declareLambda</keyword>
        <keyword>declareNLambda</keyword>
        <keyword>declareSQNLambda</keyword>
        <keyword>defdynamic</keyword>
        <keyword>defglobalfun</keyword>
        <keyword>define</keyword>
        <keyword>define_syntax</keyword>
        <keyword>defmacro</keyword>
        <keyword>defsetf</keyword>
        <keyword>defun</keyword>
        <keyword>defUserInitProc</keyword>
        <keyword>destructuringBind</keyword>
        <keyword>dynamic</keyword>
        <keyword>dynamicLet</keyword>
        <keyword>err</keyword>
        <keyword>error</keyword>
        <keyword>errset</keyword>
        <keyword>errsetstring</keyword>
        <keyword>eval</keyword>
        <keyword>evalstring</keyword>
        <keyword>expandMacro</keyword>
        <keyword>fboundp</keyword>
        <keyword>flet</keyword>
        <keyword>funcall</keyword>
        <keyword>getd</keyword>
        <keyword>getFnWriteProtect</keyword>
        <keyword>getFunType</keyword>
        <keyword>getVarWriteProtect</keyword>
        <keyword>globalProc</keyword>
        <keyword>isCallable</keyword>
        <keyword>isMacro</keyword>
        <keyword>labels</keyword>
        <keyword>lambda</keyword>
        <keyword>let</keyword>
        <keyword>letrec</keyword>
        <keyword>letseq</keyword>
        <keyword>mprocedure</keyword>
        <keyword>nlambda</keyword>
        <keyword>nprocedure</keyword>
        <keyword>procedure</keyword>
        <keyword>procedurep</keyword>
        <keyword>prog</keyword>
        <keyword>prog1</keyword>
        <keyword>prog2</keyword>
        <keyword>progn</keyword>
        <keyword>putd</keyword>
        <keyword>setf_dynamic</keyword>
        <keyword>setFnWriteProtect</keyword>
        <keyword>setVarWriteProtect</keyword>
        <keyword>unalias</keyword>
        <keyword>unwindProtect</keyword>
        <keyword>warn</keyword>

        <!-- 14 Environment Functions -->
        <keyword>cdsGetInstPath</keyword>
        <keyword>cdsGetToolsPath</keyword>
        <keyword>cdsPlat</keyword>
        <keyword>changeWorkingDir</keyword>
        <keyword>cputime</keyword>
        <keyword>createDir</keyword>
        <keyword>createDirHier</keyword>
        <keyword>csh</keyword>
        <keyword>deleteDir</keyword>
        <keyword>deleteFile</keyword>
        <keyword>exit</keyword>
        <keyword>getCurrentTime</keyword>
        <keyword>getInstallPath</keyword>
        <keyword>getLogin</keyword>
        <keyword>getPrompts</keyword>
        <keyword>getShellEnvVar</keyword>
        <keyword>getSkillPath</keyword>
        <keyword>getTempDir</keyword>
        <keyword>getWorkingDir</keyword>
        <keyword>isDir</keyword>
        <keyword>prependInstallPath</keyword>
        <keyword>setShellEnvVar</keyword>
        <keyword>setSkillPath</keyword>
        <keyword>sh|shell</keyword>
        <keyword>system</keyword>
        <keyword>unsetShellEnvVar</keyword>
        <keyword>vi|vii|vil</keyword>

        <!-- 15 Namespace Functions -->
        <keyword>makeNamespace</keyword>
        <keyword>findNamespace</keyword>
        <keyword>useNamespace</keyword>
        <keyword>unuseNamespace</keyword>
        <keyword>importSymbol</keyword>
        <keyword>findSymbol</keyword>
        <keyword>addToExportList</keyword>
        <keyword>getSymbolNamespace</keyword>
        <keyword>removeFromExportList</keyword>
        <keyword>addToNamespace</keyword>
        <keyword>shadow</keyword>
        <keyword>shadowImport</keyword>
        <keyword>removeShadowImport</keyword>
        <keyword>unimportSymbol</keyword>

        <!-- 17 Mapping Symbols to Values -->

        <!-- 18 setf Helper Functions -->

        <!-- unbound -->
        <keyword>unbound</keyword>
    </context>

    <context id="function" style-ref="function">
      <prefix>(?&lt;![\w\d_])</prefix>
      <suffix>(?![\w\d_])</suffix>
        <!-- 1 List Functions -->
        <keyword>append</keyword>
        <keyword>append1</keyword>
        <keyword>c[ad]+r</keyword>
        <keyword>cons</keyword>
        <keyword>constar</keyword>
        <keyword>copy</keyword>
        <keyword>dtpr</keyword>
        <keyword>last</keyword>
        <keyword>lconc</keyword>
        <keyword>length</keyword>
        <keyword>lindex</keyword>
        <keyword>list</keyword>
        <keyword>listp</keyword>
        <keyword>nconc</keyword>
        <keyword>ncons</keyword>
        <keyword>nth</keyword>
        <keyword>nthcdr</keyword>
        <keyword>nthelem</keyword>
        <keyword>pairp</keyword>
        <keyword>range</keyword>
        <keyword>remd</keyword>
        <keyword>remdq</keyword>
        <keyword>remove</keyword>
        <keyword>removeListDuplicates</keyword>
        <keyword>remq</keyword>
        <keyword>reverse</keyword>
        <keyword>rplaca</keyword>
        <keyword>rplacd</keyword>
        <keyword>setcar</keyword>
        <keyword>setcdr</keyword>
        <keyword>tailp</keyword>
        <keyword>tconc</keyword>
        <keyword>xcons</keyword>
        <keyword>xCoord</keyword>
        <keyword>yCoord</keyword>

        <!-- 2 Data Structure -->
        <keyword>arrayp</keyword>
        <keyword>arrayref</keyword>
        <keyword>assoc|assq|assv</keyword>
        <keyword>declare</keyword>
        <keyword>defprop</keyword>
        <keyword>defstruct</keyword>
        <keyword>defstructp</keyword>
        <keyword>defvar</keyword>
        <keyword>makeTable</keyword>
        <keyword>makeVector</keyword>
        <keyword>setarray</keyword>
        <keyword>tablep</keyword>
        <keyword>type|typep</keyword>
        <keyword>vector</keyword>
        <keyword>vectorp</keyword>

        <!-- 3 Data Operator Functions -->
        <keyword>alphaNumCmp</keyword>
        <keyword>concat</keyword>
        <keyword>copy_\w+</keyword>
        <keyword>copyDefstructDeep</keyword>
        <keyword>get</keyword>
        <keyword>getSG</keyword>
        <keyword>getq|\.</keyword>
        <keyword>getqq</keyword>
        <keyword>importSkillVar</keyword>
        <keyword>integerp</keyword>
        <keyword>make_\w+</keyword>
        <keyword>otherp</keyword>
        <keyword>plist</keyword>
        <keyword>popf</keyword>
        <keyword>postArrayDec</keyword>
        <keyword>postArrayInc</keyword>
        <keyword>postArraySet</keyword>
        <keyword>postdecrement</keyword>
        <keyword>postincrement</keyword>
        <keyword>preArrayDec</keyword>
        <keyword>preArrayInc</keyword>
        <keyword>preArraySet</keyword>
        <keyword>predecrement</keyword>
        <keyword>preincrement</keyword>
        <keyword>pushf</keyword>
        <keyword>putprop</keyword>
        <keyword>putpropq</keyword>
        <keyword>putpropqq</keyword>
        <keyword>quote</keyword>
        <keyword>remprop</keyword>
        <keyword>rotatef</keyword>
        <keyword>set</keyword>
        <keyword>setf</keyword>
        <keyword>setf_\w+</keyword>
        <keyword>setguard</keyword>
        <keyword>setplist</keyword>
        <keyword>setq</keyword>
        <keyword>setSG</keyword>
        <keyword>symbolp</keyword>
        <keyword>symeval</keyword>
        <keyword>symstrp</keyword>

        <!-- 4 Type Conversion Functions -->
        <keyword>charToInt</keyword>
        <keyword>intToChar</keyword>
        <keyword>listToVector</keyword>
        <keyword>stringToFunction</keyword>
        <keyword>stringToSymbol</keyword>
        <keyword>stringToTime</keyword>
        <keyword>symbolToString</keyword>
        <keyword>tableToList</keyword>
        <keyword>timeToString</keyword>
        <keyword>timeToTm</keyword>
        <keyword>tmToTime</keyword>
        <keyword>vectorToList</keyword>

        <!-- 5 String Functions -->
        <keyword>blankstrp</keyword>
        <keyword>buildString</keyword>
        <keyword>getchar</keyword>
        <keyword>index</keyword>
        <keyword>lowerCase</keyword>
        <keyword>lsprintf</keyword>
        <keyword>nindex</keyword>
        <keyword>outstringp</keyword>
        <keyword>parseString</keyword>
        <keyword>pcreCompile</keyword>
        <keyword>pcreExecute</keyword>
        <keyword>pcreGenCompileOptBits</keyword>
        <keyword>pcreGenExecOptBits</keyword>
        <keyword>pcreGetRecursionLimit</keyword>
        <keyword>pcreListCompileOptBits</keyword>
        <keyword>pcreListExecOptBits</keyword>
        <keyword>pcreMatchAssocList</keyword>
        <keyword>pcreMatchList</keyword>
        <keyword>pcreMatchp</keyword>
        <keyword>pcreObjectp</keyword>
        <keyword>pcrePrintLastMatchErr</keyword>
        <keyword>pcreReplace</keyword>
        <keyword>pcreSetRecursionLimit</keyword>
        <keyword>pcreSubpatCount</keyword>
        <keyword>pcreSubstitute</keyword>
        <keyword>readstring</keyword>
        <keyword>rexCompile</keyword>
        <keyword>rexExecute</keyword>
        <keyword>rexMagic</keyword>
        <keyword>rexMatchAssocList</keyword>
        <keyword>rexMatchList</keyword>
        <keyword>rexMatchp</keyword>
        <keyword>rexReplace</keyword>
        <keyword>rexSubstitute</keyword>
        <keyword>rindex</keyword>
        <keyword>sprintf</keyword>
        <keyword>strcat</keyword>
        <keyword>strcmp</keyword>
        <keyword>stringp</keyword>
        <keyword>strlen</keyword>
        <keyword>strncat</keyword>
        <keyword>strncmp</keyword>
        <keyword>strpbrk</keyword>
        <keyword>subst</keyword>
        <keyword>substring</keyword>
        <keyword>upperCase</keyword>

        <!-- 6 Arithmetic Functions -->
        <keyword>abs</keyword>
        <keyword>add1</keyword>
        <keyword>atof</keyword>
        <keyword>atoi</keyword>
        <keyword>ceiling</keyword>
        <keyword>defMathConstants</keyword>
        <keyword>difference</keyword>
        <keyword>evenp</keyword>
        <keyword>exp</keyword>
        <keyword>expt</keyword>
        <keyword>fix</keyword>
        <keyword>fixp</keyword>
        <keyword>fix2</keyword>
        <keyword>float</keyword>
        <keyword>floatp</keyword>
        <keyword>floor</keyword>
        <keyword>int</keyword>
        <keyword>isInfinity</keyword>
        <keyword>isNaN</keyword>
        <keyword>leftshift</keyword>
        <keyword>log</keyword>
        <keyword>log10</keyword>
        <keyword>max</keyword>
        <keyword>min</keyword>
        <keyword>minus</keyword>
        <keyword>minusp</keyword>
        <keyword>mod</keyword>
        <keyword>modf</keyword>
        <keyword>modulo</keyword>
        <keyword>nearlyEqual</keyword>
        <keyword>negativep</keyword>
        <keyword>oddp</keyword>
        <keyword>onep</keyword>
        <keyword>plus</keyword>
        <keyword>plusp</keyword>
        <keyword>quotient</keyword>
        <keyword>random</keyword>
        <keyword>realp</keyword>
        <keyword>remainder</keyword>
        <keyword>rightshift</keyword>
        <keyword>round</keyword>
        <keyword>round2</keyword>
        <keyword>sort</keyword>
        <keyword>sortcar</keyword>
        <keyword>sqrt</keyword>
        <keyword>srandom</keyword>
        <keyword>sub1</keyword>
        <keyword>times</keyword>
        <keyword>truncate</keyword>
        <keyword>xdifference</keyword>
        <keyword>xplus</keyword>
        <keyword>xquotient</keyword>
        <keyword>xtimes</keyword>
        <keyword>zerop</keyword>
        <keyword>zxtd</keyword>

        <!-- 7 Bitwise Operator Functions -->
        <keyword>band</keyword>
        <keyword>bitfield</keyword>
        <keyword>bitfield1</keyword>
        <keyword>bnand</keyword>
        <keyword>bnor</keyword>
        <keyword>bnot</keyword>
        <keyword>bor</keyword>
        <keyword>bxnor</keyword>
        <keyword>bxor</keyword>
        <keyword>setqbitfield</keyword>
        <keyword>setqbitfield1</keyword>

        <!-- 8 Trigonometric Functions -->
        <keyword>asin</keyword>
        <keyword>atan</keyword>
        <keyword>atan2</keyword>
        <keyword>cos</keyword>
        <keyword>sin</keyword>
        <keyword>tan</keyword>
        <keyword>acos</keyword>

        <!-- 9 Logical and Relational Functions -->
        <keyword>alphalessp</keyword>
        <keyword>and</keyword>
        <keyword>compareTime</keyword>
        <keyword>eq</keyword>
        <keyword>equal</keyword>
        <keyword>eqv</keyword>
        <keyword>geqp</keyword>
        <keyword>greaterp</keyword>
        <keyword>leqp</keyword>
        <keyword>lessp</keyword>
        <keyword>member|memq|memv</keyword>
        <keyword>neq</keyword>
        <keyword>nequal</keyword>
        <keyword>null</keyword>
        <keyword>numberp</keyword>
        <keyword>or</keyword>
        <keyword>sxtd</keyword>
    </context>

    <define-regex id="escaped-character" extended="true">
        \\                          # leading backslash
        (?:
            [\\\"\'nrbtfav\?]   |   # escaped character
            [0-7]{1,3}          |   # one, two, or three octal digits
            x[0-9A-Fa-f]+           # 'x' followed by hex digits
        )
    </define-regex>

    <define-regex id="binary" extended="true">
        (?&lt;![\w\.]) 0 [bB] [01]+ (?![\w\.])
    </define-regex>

    <context id="binary" style-ref="def:base-n-integer">
        <match>\%{binary}</match>
    </context>

    <define-regex id="float" extended="true">
        \b
        (
            [0-9]+ (?:[eE][-+]?[0-9]+|[YZEPTGMKk%munpfazy]) |
            (?:
                [0-9]* \. [0-9]+ |
                [0-9]+ \.
            ) (?:[eE][-+]?[0-9]+|[YZEPTGMKk%munpfazy])?
        )
        \b
    </define-regex>

    <context id="float" style-ref="def:floating-point">
        <match>\%{float}</match>
    </context>

    <define-regex id="decimal" extended="true">
        (?&lt;![\w\.]) [0-9][0-9]* (?![\w\.])
    </define-regex>

    <context id="decimal" style-ref="def:decimal">
        <match>\%{decimal}</match>
    </context>

    <context id="printf" style-ref="printf" extend-parent="false">
        <match extended="true">
            \%\%|\%
            [-]?               # flags
            (?:[1-9][0-9]*)?   # width
            (?:\.(?:[0-9]+))?  # precision
            [doxfegscnPBNLA]   # conversion specifier
        </match>
    </context>
    <context id="operator" style-ref="def:operator">
        <match extended="true">
            (?:
                -&gt;|~&gt;                 | # getqq
                [&amp;\|!=&lt;&gt;]{1,2}    |
                [~\^!/*+-]{1,2}             |
                ,|,@|`                      |
                @(key|optional|rest|aux)    |
                %|\$|\#|\:                  |
                [{}\[\]]
            )
        </match>
    </context>

<!-- Comments -->
    <context id="line-comment" style-ref="comment" end-at-line-end="true">
      <start>;</start>
      <include>
        <context ref="def:in-comment"/>
      </include>
    </context>

    <context id="multiline-comment" style-ref="comment">
        <start>/\*</start>
        <end>\*/</end>
        <include>
            <context ref="def:in-comment"/>
        </include>
    </context>

    <context id="close-comment-outside-comment" style-ref="error">
        <match>\*/(?!\*)</match>
    </context>

    <context id="escaped-character" style-ref="escaped-character">
        <match>\%{escaped-character}</match>
    </context>

    <context id="symbol" style-ref="def:type">
      <prefix>(?&lt;![\w\d_])'</prefix>
      <suffix>(?![\w\d_])</suffix>
      <keyword>(?:\%{escaped-character}|\w_)+</keyword>
    </context>

    <context id="string" style-ref="string">
      <start>"</start>
      <end>"</end>
      <include>
        <!-- <context ref="def:escape"/> -->
        <context ref="printf"/>
        <context ref="escaped-character"/>
        <context ref="def:line-continue"/>
      </include>
    </context>

    <context id="boolean-value" style-ref="boolean">
      <keyword>(t|nil)</keyword>
    </context>

  </definitions>
</language>

