## Create a new language definition
gedit uses [GtkSourceView](https://web.archive.org/web/20111227080157/http://live.gnome.org/GtkSourceView) for rendering text and [GtkSourceView](https://web.archive.org/web/20111227080157/http://live.gnome.org/GtkSourceView) maintains the language definitions for highlighting.

The preinstalled language files are usually located in `/usr/share/gtksourceview-3.0/language-specs`. Custom user languages are usually placed in `~/.local/share/gtksourceview-3.0/language-specs`. 

> Note: replace 3 with 2 in the path for [GtkSourceView](https://web.archive.org/web/20111227080157/http://live.gnome.org/GtkSourceView) version 2.

The easiest way to start a new language definition is to copy a preinstalled language definition from a language that has similar constructs as the one you want to write a spec for. You can copy and rename a file from the systems directory to the local user (create the directory if it doesn't exist yet) one and edit the file accordingly.

The important thing you need to change are the id and name of the language and the metadata properties mimetypes and globs in the language spec. These should resemble your new language. It might be that your files do not have an appropriate mimetype associated yet. You can either in that case leave it empty, or add a new mimetype (see below).

The format of the language file can be found in the documentation on library.gnome.org: [Specification Tutorial](https://web.archive.org/web/20111227080157/http://developer.gnome.org/gtksourceview/stable/lang-tutorial.html) and [Specification Reference](https://web.archive.org/web/20111227080157/http://developer.gnome.org/gtksourceview/stable/lang-reference.html).

Once you're done editing the language file, gedit needs to be restarted to notice the changes. Make sure to close all windows of gedit before restarting it. Trick: with gedit 3, you can take advantage of the new command line option -s, allowing to run gedit in standalone mode. So you can edit your files in a gedit instance, and test them in another running `gedit -s` on a terminal.

If your files have appropriate mime types specified and everything is well, then the highlighting should be activated when you open your language files. If gedit is not able to autodetect the language from the files mime type you may still select the language from the **View -> Highlighting Mode** menu.

If for some reason your language doesn't show up in gedit, it might be a good idea to start gedit from a terminal window and see if any errors/warnings appear for your language files. This usually gives good clues what's wrong with the specification.

## Adding a new mime type
To have gedit detect automatically which highlighting to apply to a file, it is necessary to map a mime type to your files. If your file doesn't yet have a mime type specified then you need to create a new one. It is good practice to also put the custom user mime types in a local user directory. For the mime type database this is: `~/.local/share/mime`. You will need to add a new mime type specification file in the directory `~/.local/share/mime/packages` (create it if it doesn't exist yet). If the language for instance is called foo then it is good practice to add a file foo.xml in that directory which looks like this:

```html
<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>
  <mime-type type="text/x-foo">
    <comment>Foo Source</comment>
    <!-- more translated comment elements -->
    <glob pattern="*.foo"/>
  </mime-type>
</mime-info>
```

Important here is to specify a type (text/x-foo) and the glob pattern to match the file to the mime type. Once you've created the file, go to the directory `~/.local/share` and run the following:

```sh
update-mime-database mime
```

The mime types should now be registered and after a restart of gedit your language should automatically be mapped to the correct files.

> ## Notes
> * The directory paths are based on an Ubuntu system and might be different for other distributions.
> * See [GtkSourceView/LanguageDefinitions](https://web.archive.org/web/20111227080157/http://live.gnome.org/GtkSourceView/LanguageDefinitions) for a list of third party language definitions.
> * Other language specs see: [GNOME GitLab](https://gitlab.gnome.org/GNOME/gtksourceview/tree/master/data/language-specs)

# Styles

https://wiki.gnome.org/Projects/GtkSourceView/StyleSchemes
https://perso.crans.org/besson/publis/gedit-coloration/styles/

## GtkSourceView 2

A typical search path for GtkSourceView version 2 looks like:

1. ~/.local/share/gtksourceview-2.0/styles/
2. /usr/share/gnome/gtksourceview-2.0/styles/
3. /usr/local/share/gtksourceview-2.0/styles/
4. /usr/share/gtksourceview-2.0/styles/

If this does not work in your distribution you can find out your style search path with this python script. To run it you need the python bindings for GtkSourceView 2. In Debian-based distributions this is package python-gtksourceview2.

## GtkSourceView 3

A typical search path for GtkSourceView version 3 looks like:

1. ~/.local/share/gtksourceview-3.0/styles/
2. /usr/share/gnome/gtksourceview-3.0/styles/
3. /usr/local/share/gtksourceview-3.0/styles/
4. /usr/share/gtksourceview-3.0/styles/

