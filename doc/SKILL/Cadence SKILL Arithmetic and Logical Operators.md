# Arithmetic and Logical Operators

All arithmetic operators are translated into calls to predefined SKILL functions. These operators are listed in the table below in descending order of operator precedence. The table also lists the names of the functions, which can be called like any other SKILL function.

Error messages report problems using the name of the function. The letters in the Synopsis column refer to data types. Refer to the Data Types for a discussion of data type characters.

## Arithmetic and Logical Operators

Type of Operator	Name of Function(s)	Synopsis	Operator
Data Access
arrayref
a[index]
[ ]
Data Access
setarray
a[index] = expr
Data Access
bitfield1
x<bit>
<>
Data Access
setqbitfield1
x<bit>=expr
Data Access
setqbitfield
x<msb:lsb>=expr
Data Access
quote
'expr
'
Data Access
getqq
g.s
.
Data Access
getq
g->s
->
Data Access
putpropqq
g.s = expr, g->s = expr
~>
Data Access
putpropq
d~>s, d~>s = expr
Unary
preincrement
++s
++
Unary
postincrement
s++
++
Unary
predecrement
--s
--
Unary
postdecrement
s--
--
Unary
minus
-n
-
Unary
null
!expr
!
Unary
bnot
~x
~
Binary
expt
n1 ** n2
**
Binary
times
n1 * n2
*
Binary
quotient
n1 / n2
/
Binary
plus
n1 + n2
+
Binary
difference
n1 - n2
-
Binary
leftshift
x1 << x2
<<
Binary
rightshift
x1 >> x2
>>
Binary
lessp
n1<n2
<
Binary
greaterp
n1>n2
>
Binary
leqp
n1<=n2
<=
Binary
geqp
n1>=n2
>=
Binary
equal
g1 == g2
==
Binary
nequal
g1 != g2
!=
Binary
band
x1 & x2
&
Binary
bnand
x1 ~& x2
~&
Binary
bxor
x1 ^ x2
^
Binary
bxnor
x1 ~^ x2
~^
Binary
bor
x1 | x2
Binary
bnor
x1 ~| x2
|, ~|
Binary
and
rel. expr && rel. expr
&&
Binary
or
rel. expr || rel. expr
||
Binary
range
g1 : g2
:
Binary
setq
s = expr
=
Inplace Operator
plus
a = a + b
+=
Inplace Operator
minus
a = a - b
-=
Inplace Operator
divide
a = a / b
/=
Inplace Operator
multiply
a = a * b
*=
Inplace Operator
expt
a = a ** b
**=
Inplace Operator
bor
a = a | b
|=
Inplace Operator
band
a = a & b
&=
Inplace Operator
bxor
a = a ^ b
^=
Inplace Operator
leftshift
a = a<< b
<<=
Inplace Operator
riightshift
a = a >> b
>>=
The following table gives more details on some of the arithmetic operators.

## More on Arithmetic Operators

Arithmetic Operator	Comments
+, -, *, and /
Perform addition, subtraction, multiplication, and division operations.
Exponentiation operator **
Has the highest precedence among the binary operators.
Shift operators (<<, >>)
Shift their first arguments left or right by the number of bits specified by their second arguments. Both the left and right shifts are logical (that is, vacated bits are 0-filled).
Preincrement operator 
(++ appearing before the name of a variable)
Takes the name of a variable as its argument, increments its value (which must be a number) by one, stores it back into the variable, and then returns the incremented value.
Postincrement operator 
(++ appearing after the name of a variable)
Takes the name of a variable as its argument, increments its value (which must be a number) by one, and stores it back into the variable. 
However, it returns the original value stored in the variable as the result of the function call.
Predecrement and postdecrement operators
Similar to pre- and postincrement, but they decrement instead of increment the values of their arguments by one.
Range operator (:)
Evaluates both of its arguments and returns the results as a two-element list. It provides a convenient way of grouping a pair of data values for subsequent processing. For example, 1:3 returns the list (1 3).

## Predefined Arithmetic Functions

In addition to the basic infix arithmetic operators, several functions are predefined in SKILL.
Predefined Arithmetic Functions

Synopsis	Result
General Functions
add1(n)
n + 1
sub1(n)
n - 1
abs(n)
Absolute value of n
exp(n)
e raised to the power n
log(n)
Natural logarithm of n
max(n1 n2 …)
Maximum of the given arguments
min(n1 n2 …)
Minimum of the given arguments
mod(x1 x2)
x1 modulo x2, that is, the integer remainder of dividing x1 by x2
round(n)
Integer whose value is closest to n
sqrt(n)
Square root of n
sxtd(x w)
Sign-extends the rightmost w bits of x, that is, the bit field x<w-1:0> with x<w-1> as the sign bit
zxtd(x w)
Zero-extends the rightmost w bits of x, executes faster than doing x<w-1:0>
Trigonometric Functions
sin(n)
sine, argument n is in radians
cos(n)
cosine
tan(n)
tangent
asin(n)
arc sine, result is in radians
acos(n)
arc cosine
atan(n)
arc tangent
Random Number Generator
random(x)
Returns a random integer between 0 and x-1. If random is called with no arguments, it returns an integer that has all of its bits randomly set.
srandom(x)
Sets the initial state of the random number generator to x
Bitwise Logical Operators
The bnot, band, bnand, bxor, bxnor, bor, and bnor operators all perform bitwise logical operations on their integer arguments.
Bitwise Logical Operators
Meaning	Operator
bitwise AND
&
bitwise inclusive OR
|
bitwise exclusive OR
^
left shift
>>
right shift
<<
one’s complement (unary)
~

