# Data Types Supported by SKILL

Data Type	Internal Name	Single Character Mnemonic
array
array
a
Cadence database object
dbobject
d
floating-point number
flonum
f
any data type
general
g
linked list
list
l
integer or floating point number
n
user-defined type
o
I/O port
port
p
defstruct
r
relative object design (ROD) object
rodObj
R
symbol
symbol
s
symbol or character string
S
character string (text)
string
t
function object
u
window type
w
integer number
fixnum
x
binary function
binary
y



Radix	Prefix	Examples [value in decimal]
binary
0b or 0B
0b0011 [ 3 ] 
0b0010 [ 2 ]

octal
0
077 [ 63 ] 
011 [ 9 ] 
067 => 55 (octal value)
099 => 99 (decimal)

hexadecimal
0x or 0X
0x3f [ 63 ] 
0xff [ 255 ]

Scaling Factors
Character	Name	Multiplier	Examples
Y
Yotta
1024
10Y [ 10e+25 ]
Z
Zetta
1021
10Z [ 10e+22 ]
E
Exa
1018
10E [ 10e+19 ]
P
Peta
1015
10P [ 10e+16 ]
T
Tera
1012
10T [ 10e+13 ]
G
Giga
109
10G [ 10,000,000,000 ]
M
Mega
106
10M [ 10,000,000 ]
k or K
kilo
103
10k [ 10,000 ]
%
percent
10-2
5% [ 0.05 ]
m
milli
10-3
5m [ 5.0e-3 ]
u
micro
10-6
1.2u [ 1.2e-6 ]
n
nano
10-9
1.2n [ 1.2e-9 ]
p
pico
10-12
1.2p [ 1.2e-12 ]
f
femto
10-15
1.2f [ 1.2e-15 ]
a
atto
10-18
1.2a [ 1.2e-18 ]
z
zepto
10-21
1.2z [ 1.2e-21 ]
y
yocto
10-24
1.2y [ 1.2e-24 ]



Escape Sequences
Character	Escape Sequence
new-line (line feed)
"\n"
horizontal tab
"\t"
vertical tab
"\v"
backspace
"\b"
carriage return
"\r"
form feed
"\f"
backslash
"\\"
double quote
"\”"
ASCII code ddd (octal)
"\000 \123 \456 \13"


Symbols for ASCII Characters
Character(s)	SKILL Symbol Name(s)
a, b, …, z
a, b, …, z
A, B, …, Z
A, B, …, Z
?,  _
?,  _
0, 1, …, 9
\0, \1, …, \9
^A, ^B, …, ^Z, …
(octal codes 001-037)
\001, \002, …, \032, …
(in octal)
<space>
\<space> (backslash followed by a space)
!   .   ;   :   ,
\!  \.  \;  \:  \,
(  )  [  ]  {  }
\(  \)  \[  \]  \{  …
“  #  %  &  +  -  *
\“  \#  \% …
<  =  >  @  /  \  ^  ~
\<  \=  \>  …
DEL
\177

