SVRF rule file
    +Layer Assignments
    +Global Layer Definitions
    +Comments
    +Include Statements
    +Rule Check Statements {
        +Local Layer Definitions
        +Layer Operations
        +Rule Check Comments
    }
    -Specification Statements
    -Connectivity Extraction Operations
    -Parasitic Extraction Statements
    -Device Recognition Operations
    -Conditionals
    ?Macros
    -Runtime TVF Functions


Rexes:

s/^([A-Z ]+)([a-z]+)(.+)$/$1\U$2$3\n$1$3/mg
s/\n/|/mg
