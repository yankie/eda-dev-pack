# Math Operators

## Unary Operators

Takes one argument.

| Operator | Definition                                                                    |
|----------|-------------------------------------------------------------------------------|
| `+`      | indicates a positive number                                                   |
| `-`      | indicates a negative number                                                   |
| `!`      | returns 1 (true) if its argument is 0; returns 0 (false) otherwise            |
| `~`      | returns 1 (true) if its argument is non-positive; returns 0 (false) otherwise |


## Binary Operators

Takes two arguments.

| Operator | Definition     | Precedence | Usage                     |
|----------|----------------|------------|---------------------------|
| `^`      | power function | 1          | unconditional expressions |
| `*`      | multiplication | 2          | unconditional expressions |
| `/`      | division       | 2          | unconditional expressions |
| `+`      | addition       | 3          | unconditional expressions |
| `-`      | subtraction    | 3          | unconditional expressions |
| `&&`     | AND            | 4          | conditional expressions   |
| `\|\|`   | OR             | 4          | conditional expressions   |

> Note that this operator has the same precedence as * and / in other SVRF operations. Values of x and y for which results are undefined generate an error. 
> 
## Ternary Conditional Operator

Takes three arguments.

`(condition) ? if-expression : else-expression`

# Functions

* Supported Math Functions: file:///grid/cad_tools/mentor/docs_cal_2021.1_16.10/docs/htmldocs/svrf_ur/topics/General_MathFunctions_id7c9c3927.html

* Reference for DFM Functions: file:///grid/cad_tools/mentor/docs_cal_2021.1_16.10/docs/htmldocs/svrf_ur/topics/Contain_ReferenceForDfmFunctions_id42c1fcb0.html
