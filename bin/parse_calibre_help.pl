: # -*- perl -*-
eval 'exec perl -w -S $0 ${1+"$@"}'
  if 0;

###############################################################################
# @Author: pefimov
# @Date:   2023-01-01 10:00:00
# @Last Modified by:   Pavel Efimov
# @Last Modified time: 2023-06-20 12:00:00
###############################################################################

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';

use Mojo::DOM;

use Data::Dumper;
# Remove parent link and sort hash keys
$Data::Dumper::Sortkeys = sub { [ sort grep { !/^parent$/ } keys %{$_[0]} ] };

my $text;
{
  local $/;
  $text .= <>;
}
my $primary_keywords = qr/^[\w#]/i;
# $primary_keywords = qr/^(DFM FUNCTION|Coincident|DENSITY|DENSITY CONVOLVE|DFM EXPAND ENCLOSURE|DFM EXPAND EDGE|DFM GROW|DFM JOGCLEAN|DFM SHIFT|DFM SPEC FILL|Internal)/i;
# $primary_keywords = qr/^(Angle|Not Angle|Coincident Edge|Not Coincident Edge|Coincident Inside Edge|Not Coincident Inside Edge|Coincident Outside Edge|Not Coincident Outside Edge|Convex Edge|Dfm Classify|Dfm Measure|Dfm Or Edge|Dfm Segment|Dfm Shift Edge|Dfm Stamp|Inside Edge|Not Inside Edge|Length|Not Length|Or Edge|Outside Edge|Not Outside Edge|Path Length|Touch Edge|Not Touch Edge|Touch Inside Edge|Not Touch Inside Edge|Touch Outside Edge|Not Touch Outside Edge|Dfm Classify|Dfm Dv|Dfm Shift Edge|Dfm Space|Drawn Acute|Drawn Angled|Drawn Offgrid|Drawn Skew|Offgrid|Offgrid Directional|And|Area|Not Area|Cut|Not Cut|Deangle|Density|Density Convolve|Device Layer|Dfm Analyze|Dfm Caf|Dfm Circle Analyze|Dfm Classify|Dfm Contour|Dfm Spec Contour|Dfm Copy|Dfm Create Layer Polygons|Dfm Critical Area|Dfm Density|Dfm Dv Override|Dfm Dv Analyze|Dfm Expand Edge|Dfm Expand Enclosure|Dfm Fill|Dfm Gca|Dfm Grow|Dfm Histogram|Dfm Jogclean|Dfm Measure|Dfm Narac|Dfm Optimize|Dfm Or|Dfm Partition|Dfm Pattern Capture|Dfm Pattern Classify|Dfm Property|Dfm Property Merge|Dfm Text|Dfm Expand Edge|Dfm Copy|Dfm Read|Dfm Property Net|Dfm Property Select Secondary|Dfm Property Singleton|Dfm Rdb|Dfm Rdb Ascii|Dfm Rdb Def|Dfm Read|Dfm Rectangles|Dfm Redundant Vias|Dfm Spec Via Redundancy|Dfm Remove Edge|Dfm Select By Net|Dfm Shift|Dfm Size|Dfm Stamp|Dfm Stripe|Dfm Text|Dfm Transform|Dfm Transition|Donut|Not Donut|Enclose|Not Enclose|Enclose Rectangle|Not Enclose Rectangle|Expand Edge|Expand Text|Extent|Extent Cell|Extent Drawn|Extents|Grow|Holes|Inside|Not Inside|Inside Cell|Not Inside Cell|Interact|Not Interact|Magnify|Net|Not Net|Net Area|Net Area Ratio|Net Area Ratio Accumulate|Net Area Ratio Coupled|Net Interact|Not|Or|Ornet|Outside|Not Outside|Pathchk|Perimeter|Pins|Ports|Push|Rectangle|Not Rectangle|Rectangles|Rotate|Shift|Shrink|Size|Snap|Stamp|Topex|Touch|Not Touch|Vertex|With Edge|Not With Edge|With Neighbor|Not With Neighbor|With Text|Not With Text|With Width|Not With Width|Xor|Copy|Dfm Merge|Flatten|Inductance Micheck|Merge|Tvf Layer Operation|Dfm Space|Enclosure|External|Internal|Rectangle Enclosure|Tddrc|drc)/i;
# $primary_keywords = qr/^(AND|ANGLE|AREA|ATTACH|CAPACITANCE|COINcident|CONNECT|CONVEX|COPY|CUT|DEANGLE|DENSITY|DEVice|DFM|DISCONNECT|DONUT|DRAWN|DRC|ENCLOSE|ENClosure|ERC|EXCLUDE|EXPAND|EXTENT|EXTENTS|EXTernal|FLAG|FLATTEN|FRACTURE|GROUP|GROW|HCELL|HOLES|INCLUDE|INDUCTANCE|INside|INTERACT|INTernal|LABEL|LAYER|LAYOUT|LENGTH|LITHO|LVS|MAGNIFY|MASK|MDP|MDPMERGE|MDPSTAT|MDPVERIFY|MERGE|NET|NOT|OFFGRID|OPCBIAS|OPCLINEEND|OPCSBAR|OR|ORNET|OUTside|PARASITIC|PATH|PATHCHK|PERC|PERIMETER|PEX|PINS|POLYGON|PORT|PORTS|PRECISION|PUSH|RECTANGLE|RECTANGLES|RESISTANCE|RESOLUTION|RET|ROTATE|SCONNECT|SHIFT|SHRINK|SIZE|SNAP|SOURCE|STAMP|SVRF|TDDRC|TEXT|TITLE|TOPEX|TOUCH|TRACE|UNIT|VARIABLE|VERTEX|VIRTUAL|WITH|XOR)/i;

my $dom = Mojo::DOM->new( $text );

my @keywords = ();
open SECFILE, ">>__secondary_keywords.syn" or die;

my $title = $dom->at('h1.title')->text;

if ($title =~ $primary_keywords) {
    $title =~ s/[^\w\:\_]+/ /gm;
    $title = lc $title;

    my $fname = $title;
    $fname =~ s/ /_/gm;
    print "title: ", $title,"\n";
    open FILE, ">$fname.syntax" or die;
    # TODO:
    # Seem that Calibre Docs uses following css-classes in addition to "keyword ParameterName" :
    # * .Required - SVRF keywords
    # * .Optional - SVRF keywords
    # * .OptionalDefault - for default keywords
    # * .RequiredReplaceable - values
    # * .OptionalReplaceable - values
    # Fix '_' in regex to process "PERIMeter_COincide"

    foreach my $str ($dom->find('p.lines.UsageLine')->map('all_text')->each) {
        # print $str, "\n";
        $str =~ s/[\s\h\v\xa0]+/ /ugm;
        $str =~ s/[^a-z0-9_ \|\[\{\}\]\(\)\:]+/ /iugm;
        $str =~ s/  +/ /ugm;

        $str =~ s/\| +/|/ugm;
        $str =~ s/ +\|/|/ugm;
        # $str =~ s/\{/(?:/ugm;
        # $str =~ s/\}/)/ugm;
        # $str =~ s/\[/(?:/ugm;
        # $str =~ s/\]/|)/ugm;
        # print "\t", $str, "\n";
        if( $str =~ /^($title)/i ) {
          $title = $1;
          # print "\ttitle: ", $title,"\n";
          print FILE "usage: ", $str, "\n\n";
        }
        else {
          print FILE $str, "\n\n";
        }

        $str =~ s/^$title//i;
        while (
          $str =~ s/([^\[\]\{\}\(\)]+)// or 
          $str =~ s/[\[]([^\[\]]+)[\]]// or
          $str =~ s/[\{]([^\{\}]+)[\}]// or
          $str =~ s/[\(]([^\(\)]+)[\)]//
        ) {
          # print "\t\t", $1, "\n";
          push 
            @keywords,
            grep
              /^[A-Z]/,
              map {
                # print "\t\t\tclean-up:> $_\n";
                $_ =~ s/\s+/ /mg;
                $_ =~ s/[^a-z0-9_ \(\?:\|\)]+//img;
                $_ =~ s/[A-Z_-]+://;
                # $_ =~ s/\b[A-Za-z][a-z0-9_][\w]*\b/\n/mg;
                $_ =~ s/^\s+//mg;
                $_ =~ s/\s+$//mg;
                # print "\t\t\tclean-up:= $_\n";
                $_;
              } split(/(?:\||\b[A-Za-z][a-z0-9_]*\b|\b[a-z][a-z0-9_]*[A-Z]+\b)/, $1 );
        }
        # print "\n\n";

    }
    $title =~ s/([A-Z][A-Z]+)([a-z][a-z_]+)/$1(?:$2|)/mg;
    print SECFILE "primary: $fname|: ", $title,"\n";
    close FILE;
    my %sorted_keywords;

    @sorted_keywords{map {my $s = lc $_."|"; $s =~ s/ /-/g; $s} @keywords} = map { my $s = $_; $s =~ s/([A-Z][A-Z]+)([a-z][a-z_]+)/$1(?:$2|)/mg; $s} @keywords;

    # print Dumper(\%sorted_keywords);

    # print SECFILE join ("\n", @sorted_keywords{sort keys %sorted_keywords}), "\n" if @keywords;
    print SECFILE join ("\n", map { "secondary: $_ => $sorted_keywords{$_}"} sort keys %sorted_keywords), "\n" if @keywords;
    # print join ("\n", @keywords), "\n" if @keywords;
}

close SECFILE;


## Post-process:
##
# s/^#.+\n//gm
# sort
# s/^(.+)\n\1\n/$1\n/gm
# s/^(.+[A-Z]+)([a-z]+)(.*)$/$1\U$2$3\n$1$3/gm
