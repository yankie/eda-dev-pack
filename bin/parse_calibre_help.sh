#!/usr/bin/bash

SCRIPT=$(realpath -s "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

for f in /grid/cad_tools/mentor/docs_cal_2021.1_16.10/docs/htmldocs/svrf_ur/topics/Command_*
do
    fn=`basename $f`
    if [[ $fn =~ ^Command_(DfmFunc|DFMFunc)[A-Z] ]] 
    then
        echo "Processing DFM function $fn..."
        pushd DfmFunc >> /dev/null
        cat $f | $SCRIPTPATH/parse_calibre_help.pl
        popd >> /dev/null
    elif [[ $fn =~ ^Command_BIL ]] 
    then
        echo "Processing BIL function $fn..."
        pushd BIL  >> /dev/null
        cat $f | $SCRIPTPATH/parse_calibre_help.pl
        popd >> /dev/null
    elif [[ $fn =~ ^Command_(TVF|Tvf) ]] 
    then
        echo "Processing TVF command $fn..."
        pushd TVF >> /dev/null
        cat $f | $SCRIPTPATH/parse_calibre_help.pl
        popd >> /dev/null
    else 
        echo "Processing $fn..."
        cat $f | $SCRIPTPATH/parse_calibre_help.pl
    fi
done
