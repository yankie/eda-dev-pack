#!/usr/bin/tcsh

foreach f (/grid/cad_tools/mentor/docs_cal_2021.1_16.10/docs/htmldocs/svrf_ur/topics/Command_*)
    echo "Processing $f..."
    if ( X$f =~ DfmFunc ) then
        pushd DfmFunc
        cd DfmFunc
        cat $f | `dirname $0`/parse_calibre_help.pl # > `basename $f`
        popd
    else 
        cat $f | `dirname $0`/parse_calibre_help.pl # > `basename $f`
    endif
end
