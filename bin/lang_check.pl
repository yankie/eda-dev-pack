: # -*- perl -*-
eval 'exec perl -w -S $0 ${1+"$@"}'
  if 0;

###############################################################################
# @Author: pefimov
# @Date:   2023-01-01 10:00:00
# @Last Modified by:   Pavel Efimov
# @Last Modified time: 2023-06-20 12:00:00
###############################################################################

use strict;
use warnings;
use utf8;
use feature 'unicode_strings';

use Mojo::DOM;

use Data::Dumper;
# Remove parent link and sort hash keys
$Data::Dumper::Sortkeys = sub { [ sort grep { !/^parent$/ } keys %{$_[0]} ] };

my $text;
{
  local $/;
  $text .= <>;
}

my $tst = "This is a test string\nLine 2";
my $dom = Mojo::DOM->new( $text );

foreach my $str (
        $dom->find('keyword')->map('all_text')->each,
        $dom->find('start')->map('all_text')->each,
        $dom->find('end')->map('all_text')->each,
        $dom->find('define-regex')->map('all_text')->each,
        $dom->find('match')->map('all_text')->each
    ) {
    print "Checking '$str'...\n";
    eval {
        $tst =~ $str;
        1;
    } or do {
        print STDERR "Error: $@\n", "\t", $str, "\n\n";
    }
}

